const express = require('express');
const router = express.Router();
const { subirArchivos, eliminarArchivo, descargarArchivo } = require('../controllers/archivosController');
const auth = require('../middlewares/auth')


router.post('/', auth, subirArchivos)

router.get('/:archivo', descargarArchivo,
eliminarArchivo)

module.exports = router;