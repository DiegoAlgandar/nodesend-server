const express = require('express');
const router = express.Router();
const { autenticarUsuario, usuarioAutenticado } = require('../controllers/authController');
const { check } = require('express-validator')
const auth = require('../middlewares/auth')


router.post('/', [
    check('email', 'Debe escribir un email valido').isEmail(),
    check('password', 'Debe proporcionar un password').not().isEmpty()
], autenticarUsuario)

router.get('/',
    auth,
    usuarioAutenticado)

module.exports = router;