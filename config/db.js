const mongoose = require('mongoose')
require('dotenv').config({ path: 'variables.env' })

const ConectarDB = async () => {
    try {
        await mongoose.connect(process.env.DB_URL, {
            useCreateIndex: true,
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        })
        console.log('DB Connected');
    } catch (error) {
        console.log('Error compiled');
        console.log(error);
        process.exit(1)
    }
}
module.exports = ConectarDB