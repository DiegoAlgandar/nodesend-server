const multer = require('multer')
const shortid = require('shortid')
const fs = require('fs')
const Enlaces = require('../models/enlaces')

exports.subirArchivos = async (req, res, next) => {

    const multerConfig = {
        limits: { fileSize: (req.usuario ? 1024 * 1024 * 10 : 1024 * 1024) },
        storage: filesStorage = multer.diskStorage({
            destination: (req, file, cb) => {
                cb(null, __dirname + '/../uploads')
            },

            filename: (req, file, cb) => {
                const ext = file.originalname.substring(file.originalname.lastIndexOf('.'), file.originalname.length)
                cb(null, `${shortid.generate()}${ext}`)
            }
        })
    }

    const upload = multer(multerConfig).single('archivo')

    upload(req, res, async (error) => {
        console.log(req.file);

        if (!error) {
            res.json({ archivo: req.file.filename })
        } else {
            console.log(error);
            return next();
        }
    })
}

exports.eliminarArchivo = async (req, res, next) => {
    try {
        fs.unlinkSync(__dirname + `/../uploads/${req.archivo}`)

        console.log('Se eliminó a ' + req.archivo);
    } catch (error) {
        console.log(error);
    }
}

exports.descargarArchivo = async (req, res, next) => {

    const { archivo } = req.params;
    const enlace = await Enlaces.findOne({ nombre: archivo })

    const { descargas, nombre } = enlace
    const archivoDescarga = __dirname + '/../uploads/' + nombre;

    res.download(archivoDescarga);


    if (descargas === 1) {
        req.archivo = nombre
        await Enlaces.findOneAndRemove(enlace.id)
        next()
    } else {
        enlace.descargas--;
        await enlace.save()
    }

}